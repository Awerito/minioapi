from minio import Minio

from src.config import MINIO_HOST, MINIO_ACCESS_KEY, MINIO_SECRET_KEY, MINIO_BUCKET


minio_client = Minio(
    MINIO_HOST, access_key=MINIO_ACCESS_KEY, secret_key=MINIO_SECRET_KEY
)

bucket_name = MINIO_BUCKET
