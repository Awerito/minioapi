import io

from fastapi import APIRouter, File, UploadFile, HTTPException
from minio.error import S3Error
from fastapi.responses import StreamingResponse

from src.storage import minio_client, bucket_name


router = APIRouter(tags=["Minio"], prefix="/minio")


@router.post("/upload/")
async def upload_file(file: UploadFile = File(...)):
    try:
        content = await file.read()
        minio_client.put_object(
            bucket_name,
            file.filename,
            io.BytesIO(content),
            length=len(content),
            content_type=file.content_type,
        )
        return {"filename": file.filename}
    except S3Error as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.get("/download/{file_name}")
async def download_file(file_name: str):
    try:
        response = minio_client.get_object(bucket_name, file_name)
        content_length = response.getheader("Content-Length")
        content_type = response.getheader("Content-Type")
        return StreamingResponse(
            response.stream(32 * 1024),
            media_type=content_type,
            headers={"Content-Length": content_length},
        )
    except S3Error as e:
        raise HTTPException(status_code=404, detail="File not found")


@router.delete("/delete/{file_name}")
async def delete_file(file_name: str):
    try:
        minio_client.remove_object(bucket_name, file_name)
        return {"detail": "File deleted"}
    except S3Error:
        raise HTTPException(status_code=404, detail="File not found")


@router.get("/list/")
async def list_files() -> list[str]:
    try:
        objects = minio_client.list_objects(bucket_name)
        return [obj.object_name for obj in objects]
    except S3Error as e:
        raise HTTPException(status_code=500, detail=str(e))
